# mongo with charts

docker-compose file for a mongo database server with a charts server


## requirements
- host
  - docker
  - docker-compose
  - openssl
    - `apt install openssl`

## run
- init(first time)
  - edit init.sh
  - `./init.sh`
- start
  - `docker-compose up -d`
- stop
  - `docker-compose down` 
- https by nginx reverse proxy
  - in nginx 1.14, set **Host** and **X-Forwarded-Proto**
    ```
    server{
      listen 443 ssl;
      ssl_certificate /path/to/xxx.pem;
      ssl_certificate_key /path/to/xxx.key;
      server_name xxx.xxx.com;
      location / {
        proxy_pass http://<internal ip addr>:<internal port>;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-Proto https;
      }
    }
    ```

## use
- mongo db
  - client tool: **Robo 3T**
    - user: ${MONGO_INITDB_ROOT_USERNAME}
    - password: ${MONGO_INITDB_ROOT_PASSWORD}
- mongo charts
  - https://localhost:${CHARTS_EXPORT_PORT_HTTP}
    - e-mail: ${CHARTS_ADMIN_EMAIL}
    - password: ${CHARTS_ADMIN_PASSWORD}

## reference
- https://gist.github.com/Tallic/9486fb03a1425398be3091f320d23a06
- https://gist.github.com/albertincx/86d88425c53e823ccf3b8151ffb33bbe
- https://groups.google.com/g/mongodb-user/c/GfDd8IZr9YY/m/FzYtJUcMCwAJ?pli=1
- mongo db
  - add admin
      - run mongo container first time with following enviroment
  - (optional) add db and it's user
    - TODO
- mongo charts
  - add admin
    - ```
      docker-compose exec mongo-charts \ 
      charts-cli add-user \
      --first-name "admin" \
      --last-name "admin" \
      --email "admin@chartstest.test" \
      --password "123123" \
      --role "UserAdmin"
      ```
  - https cert
    - existing cert
      - copy to data/web-certs
        - charts-https.crt
        - charts-https,key
    - self-signed cert
      - `cd data/web-certs` 
      - `openssl req -newkey rsa:4096
              -x509
              -sha256
              -days 3650
              -nodes
              -out charts-https.crt
              -keyout charts-https.key` 
- docker compose
  - start
    - `docker-compose up -d`
