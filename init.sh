#!/bin/bash

# stop when error
set -e

MONGO_IMG=mongo:4.4.11
MONGO_EXPORT_PORT=27000
MONGO_VOLUME_DB=`pwd`/data/db
MONGO_INITDB_ROOT_USERNAME=
MONGO_INITDB_ROOT_PASSWORD=
CHARTS_IMG=quay.io/mongodb/charts:v1.20.2
CHARTS_VOLUME_KEYS=`pwd`/data/keys
CHARTS_VOLUME_LOGS=`pwd`/data/logs
CHARTS_VOLUME_DB_CERTS=`pwd`/data/db-certs
CHARTS_VOLUME_WEB_CERTS=`pwd`/data/web-certs
CHARTS_SUPPORT_WIDGET_AND_METRICS=off
CHARTS_ADMIN_EMAIL=
CHARTS_ADMIN_PASSWORD=
CHARTS_EXPORT_PORT_HTTP=80
CHARTS_EXPORT_PORT_HTTPS=443

function wait_server_ready {
  sleep 5  # wait the log
  while true; do
    if [ x"`docker-compose logs mongo-charts | tail -n 5 | grep 'supervisorStarted (true)' | wc -l`" == x"1" ]; then
      echo "Started"
      break
    fi
    echo -n "."
    sleep 1
  done
  sleep 5  # wait the server real ready
}

function clear_stdin {
    while read -r -t 0; do read -r; done
}

# init
if [ -d data ]; then
  echo "Init failed: data directory already exist."
  exit 1
fi

# mongo
## admin account
clear_stdin
echo ""
echo "==============================="
echo "Init mongo db..."
echo Please enter the root username for the mongo database:
read MONGO_INITDB_ROOT_USERNAME
echo Please enter the root password for the mongo database:
read -s MONGO_INITDB_ROOT_PASSWORD
docker rm -f mongo-db_init > /dev/null 2>&1
docker run -d --name mongo-db_init \
  -e MONGO_INITDB_ROOT_USERNAME=${MONGO_INITDB_ROOT_USERNAME} \
  -e MONGO_INITDB_ROOT_PASSWORD=${MONGO_INITDB_ROOT_PASSWORD} \
  -v ${MONGO_VOLUME_DB}:/data/db \
  -p ${MONGO_EXPORT_PORT}:27017 \
  ${MONGO_IMG}
docker stop mongo-db_init
docker rm mongo-db_init

# charts
## admin account
clear_stdin
echo ""
echo "==============================="
echo "Init mongo charts..."
echo Please enter the admin e-mail for the mongo charts:
read CHARTS_ADMIN_EMAIL
echo Please enter the admin password for the mongo charts:
read -s CHARTS_ADMIN_PASSWORD

# setup .env for docker-compose
echo ""
echo "==============================="
echo "Generating .env for docker-compose..."
echo "" > .env
echo "MONGO_IMG=$MONGO_IMG" >> .env
echo "MONGO_EXPORT_PORT=$MONGO_EXPORT_PORT" >> .env
echo "MONGO_VOLUME_DB=$MONGO_VOLUME_DB" >> .env
echo "MONGO_INITDB_ROOT_USERNAME=$MONGO_INITDB_ROOT_USERNAME" >> .env
echo "MONGO_INITDB_ROOT_PASSWORD=$MONGO_INITDB_ROOT_PASSWORD" >> .env
echo "CHARTS_IMG=$CHARTS_IMG" >> .env
echo "CHARTS_VOLUME_KEYS=$CHARTS_VOLUME_KEYS" >> .env
echo "CHARTS_VOLUME_LOGS=$CHARTS_VOLUME_LOGS" >> .env
echo "CHARTS_VOLUME_DB_CERTS=$CHARTS_VOLUME_DB_CERTS" >> .env
echo "CHARTS_VOLUME_WEB_CERTS=$CHARTS_VOLUME_WEB_CERTS" >> .env
echo "CHARTS_SUPPORT_WIDGET_AND_METRICS=$CHARTS_SUPPORT_WIDGET_AND_METRICS" >> .env
#echo "CHARTS_ADMIN_EMAIL=$CHARTS_ADMIN_EMAIL" >> .env
#echo "CHARTS_ADMIN_PASSWORD=$CHARTS_ADMIN_PASSWORD" >> .env
echo "CHARTS_EXPORT_PORT_HTTP=$CHARTS_EXPORT_PORT_HTTP" >> .env
echo "CHARTS_EXPORT_PORT_HTTPS=$CHARTS_EXPORT_PORT_HTTPS" >> .env

# run docker compose
echo ""
echo "==============================="
echo "Run server..."
docker-compose up -d
wait_server_ready

# setup the admin account of mongo chart
echo ""
echo "==============================="
echo "Setup mongo charts..."
docker-compose exec mongo-charts charts-cli \
  add-user \
  --first-name "admin" \
  --last-name "admin" \
  --email "${CHARTS_ADMIN_EMAIL}" \
  --password "${CHARTS_ADMIN_PASSWORD}" \
  --role "UserAdmin"

echo ""
echo "==============================="
echo "Done!"
docker-compose ps
